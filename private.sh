#!/bin/sh
# Filter private info from sway config and getweather
# TODO: clean this up to de-jank it

sed 's/city=".*/city=""/;s/state=".*/state=""/;s/country=".*/country=""/;s/units=".*/units=""/;s/api_key=".*/api_key=""/;s/exec wlsunset.*/exec wlsunset -l LAT -L LONG -t 4500 -T 6485/' "$@"
