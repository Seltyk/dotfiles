# minigit plugin

The minigit plugin provides five useful [functions](#functions), stolen from the git plugin.

To use it, add `minigit` to the plugins array in your zshrc file:

```zsh
plugins=(... minigit)
```

### Main branch preference

I don't actually care about branches being named `master` or `main` but I'm too lazy to change what's here so I left
the `git_main_branch` function alone. It uses `main` if that exists, `master` otherwise.

## Aliases

| Alias                  | Description                                                                       |
|:-----------------------|:----------------------------------------------------------------------------------|
| gunignore              | git update-index --no-assume-unchanged                                            |
| gwch                   | git whatchanged -p --abbrev-commit --pretty=medium                                |
| gwip                   | git add -A; git rm $(git ls-files --deleted) 2> /dev/null;                        |
|                        |     git commit --no-verify --no-gpg-sign -m "--wip-- [skip ci]"                   |
| gunwip                 | git log -n 1 \| grep -q -c "\-\-wip\-\-" && git reset HEAD~1                      |

## Functions

### Current

| Command                | Description                                                                       |
|:-----------------------|:----------------------------------------------------------------------------------|
| `grename <old> <new>`  | Rename `old` branch to `new`, including in origin remote                          |
| current_branch         | Return the name of the current branch                                             |
| git_main_branch        | Returns the name of the main branch: `main` if it exists, `master` otherwise      |

### Work in Progress (WIP)

These features allow to pause a branch development and switch to another one (_"Work in Progress"_, or wip).
When you want to go back to work, just unwip it.

| Command                | Description                                                                       |
|:-----------------------|:----------------------------------------------------------------------------------|
| work_in_progress       | Echoes a warning if the current branch is a wip                                   |
| gwip                   | Commit wip branch                                                                 |
| gunwip                 | Uncommit wip branch                                                               |
