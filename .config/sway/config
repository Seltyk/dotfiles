# Sway config file
#
# This config file uses keycodes (bindsym) and was written for the QWERTY
# layout.
#
# To get a config file with the same key positions, but for your current
# layout... idk

# Making OBS shut up
exec dbus-update-activation-environment DISPLAY I3SOCK SWAYSOCK WAYLAND_DISPLAY XDG_CURRENT_DESKTOP=sway

# Tile like metric paper
exec autotiling

# Font for window titles. Will also be used by the bar unless a different font
# is used in the bar {} block below.
font pango:Liberation Mono Bold 8

#Set mod1 to LAlt and mod2 to Super
set $mod1 Mod1
set $mod2 Mod4

# use these keys for focus, movement, and resize directions when reaching for
# the arrows is not convenient
set $up k
set $down j
set $left h
set $right l

# use Mouse+$mod2 to drag floating windows to their wanted position
floating_modifier $mod2

# start a terminal
#bindsym $mod2+Return exec i3-sensible-terminal default
bindsym $mod2+Return exec st

# kill focused window
bindsym $mod2+q kill

#Stop focused process from even being handled by the CPU
#bindsym --release $mod2+F4 exec xkill

# Start bemenu as a program launcher. This hack sucks.
bindsym $mod2+d exec BEMENU_OPTS=$(grep -oP 'BEMENU_OPTS="\K[^"]+' ~/.zprofile) bemenu-run

# change focus
bindsym $mod2+$left focus left
bindsym $mod2+$down focus down
bindsym $mod2+$up focus up
bindsym $mod2+$right focus right

# alternatively, you can use the cursor keys:
bindsym $mod2+Left focus left
bindsym $mod2+Down focus down
bindsym $mod2+Up focus up
bindsym $mod2+Right focus right

# move focused window
bindsym $mod2+Shift+$left move left
bindsym $mod2+Shift+$down move down
bindsym $mod2+Shift+$up move up
bindsym $mod2+Shift+$right move right

# move focused workspace
bindsym $mod2+Ctrl+Shift+$left move workspace to output DP-1
bindsym $mod2+Ctrl+Shift+$right move workspace to output DP-2

# alternatively, you can use the cursor keys:
bindsym $mod2+Shift+Left move left
bindsym $mod2+Shift+Down move down
bindsym $mod2+Shift+Up move up
bindsym $mod2+Shift+Right move right
bindsym $mod2+Ctrl+Shift+Left move workspace to output DP-1
bindsym $mod2+Ctrl+Shift+Right move workspace to output DP-2

# split in horizontal orientation
#bindsym $mod2+h split h default
bindsym $mod2+x split h

# split in vertical orientation
#bindsym $mod2+v split v
bindsym $mod2+z split v

# split like metric paper
bindsym $mod2+c split t

# enter fullscreen mode for the focused container
bindsym $mod2+f fullscreen toggle

# change container layout (stacked, tabbed, toggle split)
#bindsym $mod2+s layout stacking default
#bindsym $mod2+Shift+q layout stacking
bindsym $mod2+w layout tabbed
bindsym $mod2+e layout toggle split

# toggle tiling / floating
bindsym $mod2+Shift+space floating toggle

# change focus between tiling / floating windows
bindsym $mod2+space focus mode_toggle

# focus the parent container
bindsym $mod2+a focus parent

# focus the child container
#bindsym $mod2+d focus child

# move the currently focused window to the scratchpad
bindsym $mod2+Shift+minus move scratchpad

# Show the next scratchpad window or hide the focused scratchpad window.
# If there are multiple scratchpad windows, this command cycles through them.
bindsym $mod2+minus scratchpad show

# Define names for default workspaces for which we configure key bindings later on.
# We use variables to avoid repeating the names in multiple places.
set $ws1 "1"
set $ws2 "2"
set $ws3 "3"
set $ws4 "4"
set $ws5 "5"
set $ws6 "6"
set $ws7 "7"
set $ws8 "8"
set $ws9 "9"
set $ws10 "0"

# switch to workspace
bindsym $mod2+1 workspace $ws1
bindsym $mod2+2 workspace $ws2
bindsym $mod2+3 workspace $ws3
bindsym $mod2+4 workspace $ws4
bindsym $mod2+5 workspace $ws5
bindsym $mod2+6 workspace $ws6
bindsym $mod2+7 workspace $ws7
bindsym $mod2+8 workspace $ws8
bindsym $mod2+9 workspace $ws9
bindsym $mod2+0 workspace $ws10

# workspace bouncing
bindsym $mod2+Shift+q workspace back_and_forth

# move focused container to workspace
bindsym $mod2+Shift+1 move container to workspace $ws1
bindsym $mod2+Shift+2 move container to workspace $ws2
bindsym $mod2+Shift+3 move container to workspace $ws3
bindsym $mod2+Shift+4 move container to workspace $ws4
bindsym $mod2+Shift+5 move container to workspace $ws5
bindsym $mod2+Shift+6 move container to workspace $ws6
bindsym $mod2+Shift+7 move container to workspace $ws7
bindsym $mod2+Shift+8 move container to workspace $ws8
bindsym $mod2+Shift+9 move container to workspace $ws9
bindsym $mod2+Shift+0 move container to workspace $ws10

# reload the configuration file
bindsym $mod2+Shift+c reload
# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod2+Shift+r restart
# exit i3 (logs you out of your X session)
#bindsym $mod2+Shift+e exec "i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -b 'Yes, exit i3' 'i3-msg exit'"
bindsym $mod2+Shift+e exec swaynag -t warning -m 'You pressed the exit shortcut. Do you really want to exit sway? This will end your Wayland session.' -b 'Yes, exit sway' 'swaymsg exit'

# resize window (you can also use the mouse for that)
mode "resize" {
        # These bindings trigger as soon as you enter the resize mode

        # Pressing left will shrink the window’s width.
        # Pressing right will grow the window’s width.
        # Pressing up will shrink the window’s height.
        # Pressing down will grow the window’s height.
        bindsym $left       resize shrink width 10 px or 10 ppt
        bindsym $down       resize grow height 10 px or 10 ppt
        bindsym $up         resize shrink height 10 px or 10 ppt
        bindsym $right      resize grow width 10 px or 10 ppt

        # same bindings, but for the arrow keys
        bindsym Left        resize shrink width 10 px or 10 ppt
        bindsym Down        resize grow height 10 px or 10 ppt
        bindsym Up          resize shrink height 10 px or 10 ppt
        bindsym Right       resize grow width 10 px or 10 ppt

        # back to normal: Enter or Escape or $mod2+r
        bindsym Return mode "default"
        bindsym Escape mode "default"
        bindsym $mod2+r mode "default"
}

bindsym $mod2+r mode "resize"

# Start i3bar to display a workspace bar (plus the system information i3status
# finds out, if available)
bar {
		status_command customstatus_weather
}

# Configure monitors
output DP-1 {
	bg /hdd/Pictures/Papes/1547790924798.png fill
	mode 3440x1440@144Hz
	pos 0 0
	adaptive_sync on
}

output DP-2 {
	bg /hdd/Pictures/Papes/Stellaris/stellaris_into_the_unknown_1920x1080.jpg fill
	mode 1920x1080@144.001007Hz
	pos 3440 360
}

# Background for right monitor is *any video I want*
#exec_always mpvpaper -fp -o "no-audio loop stream-buffer-size=8MiB" DP-2 "/hdd/Videos/Galaxy - 107129.mp4"

focus output DP-1
default_border pixel 2

# Enable typing in Greek, Russian and Japanese
input "type:keyboard" {
	xkb_layout "us-english-deadgreek",us(rus)
	xkb_options compose:menu
}
exec fcitx5

# Set cursor theme and only wake from kb
seat seat0 xcursor_theme Paper
seat seat0 idle_wake keyboard

# GTK theming
set $gnome-schema org.gnome.desktop.interface
exec_always {
	gsettings set $gnome-schema gtk-theme 'Adapta-Nokto'
	gsettings set $gnome-schema icon-theme 'Papirus-Dark'
	gsettings set $gnome-schema cursor-theme 'Paper'
}

# Sound mixer controls. Uses a Pulse mixer, but PipeWire does the work
bindsym Ctrl+$mod2+Up exec pamixer -i 5
bindsym Ctrl+$mod2+Down exec pamixer -d 5

# Custom shortcuts
bindsym --release Ctrl+Shift+4 exec grimshot save area | xargs -I {} mv '{}' "/hdd/Pictures/Scrots/$(date +%Y%m%d-%H%M%S).png"
bindsym --release Ctrl+Shift+5 exec grimshot save screen | xargs -I {} mv '{}' "/hdd/Pictures/Scrots/$(date +%Y%m%d-%H%M%S).png"
bindsym --release Ctrl+Shift+6 exec grimshot save active | xargs -I {} mv '{}' "/hdd/Pictures/Scrots/$(date +%Y%m%d-%H%M%S).png"
bindsym --release Ctrl+$mod2+4 exec grimshot copy area
bindsym --release Ctrl+$mod2+5 exec grimshot copy screen
bindsym --release Ctrl+$mod2+6 exec grimshot copy active

# Switch between ENG and RUS keyboards on the fly
bindsym Ctrl+space input "3141:25903:SONiX_USB_DEVICE" xkb_switch_layout next

# Toggle right side monitor to handle the mouse in certain games better
bindsym Ctrl+$mod2+Delete output DP-2 toggle

# Lock session
# The settings shown below have been placed in /etc/swaylock/config
bindsym Ctrl+Alt+l exec swaylock
# bindsym Ctrl+Alt+l exec swaylock -fKlc 000000 \
	#--caps-lock-key-hl-color 0077bb \
	#--indicator-radius 250 \
	#--indicator-thickness 30 \
	#--key-hl-color 55bb00 \
	#--ring-color bbffff \
	#--ring-clear-color efcd05 \
	#--ring-ver-color 29d9a4

# Because Wayland doesn't have global keybinds, here are my OBS hotkeys
bindsym Ctrl+$mod2+p exec /hdd/.local/share/cargo/bin/bosc r
bindsym Ctrl+$mod2+s exec /hdd/.local/share/cargo/bin/bosc s

# Kill displays after 5 minutes, lock after 10
# TODO: test aggressively
#exec swayidle -w \
	#timeout 300 'swaymsg "output * power off"' resume 'swaymsg "output * power on"' \
	#timeout 600 'zzz' resume 'swaylock'

# Controller inputs don't reset the idle timer. Thankfully, I rarely use my controller
# This is a known and possibly unfixable issue: https://github.com/swaywm/swayidle/issues/68
for_window [class="dolphin-emu"] inhibit_idle fullscreen
for_window [class="RushdownRevolt\.x86_64"] inhibit_idle focus; move container to output DP-2

# Assign workspaces to outputs before moving windows around
workspace $ws1 output DP-1
workspace $ws2 output DP-1
workspace $ws3 output DP-2
workspace $ws4 output DP-2
workspace $ws5 output DP-2

# Move windows to sensible places
for_window [class="Firefox"] move container to workspace $ws1
#for_window [shell="xwayland" class="^Minecraft.+$" title="^Minecraft.+$" instance="^Minecraft.+$"] \
	#move container to workspace $ws2
for_window [class="discord"] move container to workspace $ws3
#for_window [floating title="^Steam$"] move container to workspace $ws4
#for_window [class="steam"] move container to workspace $ws4, border none
for_window [class="Element"] move container to workspace $ws5
for_window [app_id="com\.obsproject\.Studio"] move container to workspace $ws6

# Fix Origin (for Titanfall 2)
for_window [instance="steam_app_1182480"] floating enable

# Keep my eyes functional
exec wlsunset -l LAT -L LONG -t 4500 -T 6485

# Start Pipewire
exec pipewire; exec wireplumber
exec pipewire-pulse

# Automount USB drives
exec udiskie -aNt --appindicator

# Start usual programs
exec firefox
exec discord-ptb
exec steam
#exec element-desktop
exec keepassxc

# Can this please work now?
exec_always xrdb /home/void/.Xresources
