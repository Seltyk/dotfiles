# Seltyk's dotfiles (and programs list)

This repo effectively covers most of the configuration files on my Void system.  
In this document you will find a rundown of these files and the tools that I use (some of which needn't have any
dotfiles)  
Some of the configuration here (particularly in .vimrc) is inspired by
[Luke Smith's dotfiles](https://github.com/LukeSmithxyz/voidrice)  
Oh, and by the way, this is *exclusively* referring to
[my Void gaming rig, nicknamed Theseus (fka HeliX)](https://pcpartpicker.com/b/4j7WGX). My system76 Gazelle laptop and
Google Pixel 7 Pro have other mixtures of software that I'm simply ignoring here.

## Critical programs I use
### Programs found here
* [Fcitx 5](https://fcitx-im.org/wiki/Fcitx_5): IM framework, specifically for typing Japanese (via Mozc)
* [git](https://git-scm.com/): version control, everybody uses this, moving on
* [sway](https://swaywm.org/): Wayland compositor. I still use i3status to customize swaybar, however
* [st](https://gitlab.com/Seltyk/st): terminal emulator. I'm not really maintaining it anymore, but it works fine
  so I don't especially care
* [vim](https://www.vim.org/): terminal-based text editor
* [zsh](https://https://www.zsh.org/): shell

### Programs not found here
* [Firefox](https://firefox.com): web browser. It's not *the best*, but it's close enough. Important add-ons:
  + [Greasemonkey](https://addons.mozilla.org/en-US/firefox/addon/greasemonkey/): helps me
    [default to GMail's HTML viewer](https://gist.github.com/wundrweapon/c79abbb3c4726d54774d88dae425d741/),
	[use "select all" in said HTML viewer](https://greasyfork.org/en/scripts/6410-gmail-basic-html-enhancement/),
    and [unfold GitHub comment threads](https://greasyfork.org/en/scripts/400462-git-hub-unroll-comments/)
  + [HTTPS Everywhere](https://addons.mozilla.org/en-US/firefox/addon/https-everywhere/): force SSL connection whenever
    possible
  + [NoScript](https://addons.mozilla.org/en-US/firefox/addon/noscript/): to be free of the internet's cancer
  + [Image Search Options](https://addons.mozilla.org/en-US/firefox/addon/image-search-options/): reverse image search
    context menu
  + [KeePassXC-Browser](https://addons.mozilla.org/en-US/firefox/addon/keepassxc-browser/): autofill passwords
  + [Return Youtube Dislike](https://addons.mozilla.org/en-US/firefox/addon/return-youtube-dislikes): tells me whether a
    video sucks before I waste time watching it
  + [Reddit Enhancement Suite (RES)](https://addons.mozilla.org/en-US/firefox/addon/reddit-enhancement-suite/): Reddit
    improvement utility. Name is metadescriptive
  + [uBlock Origin](https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/): ad blocker
  + [Video Speed Controller](https://addons.mozilla.org/en-US/firefox/addon/videospeed/): adjust playback speed of
    HTML5 videos
  + [SponsorBlock](https://addons.mozilla.org/en-US/firefox/addon/sponsorblock/): because YouTubers make money by
    annoying me, and I'm not having any of it
  + [Redirector](https://addons.mozilla.org/en-US/firefox/addon/redirector/): force Wikipedia to use the old "vector"
    skin, use BreezeWiki instead of Fandom, and use Scribe instead of Medium
  + [Consent-O-Matic](https://addons.mozilla.org/en-US/firefox/addon/consent-o-matic/): refuse tracking automatically
  + [Tree Style Tab](https://addons.mozilla.org/en-US/firefox/addon/tree-style-tab/): because I'm a freak
  + (there are others, but just... like... don't worry about it)
* [mpv](https://mpv.io/): video player
* [OBS](https://obsproject.com/): livestreaming and shadow recording utility
* [imv](https://sr.ht/~exec64/imv/): image viewer

## Non critical stuff that I use
* [Blobmoji](https://github.com/C1710/blobmoji/): blobs were the better emoji and Google should be ashamed of themselves
  for ditching it. One day I'll fork this to revive the revolver, but that day is not today.
* [Fira Code](https://github.com/tonsky/FiraCode/): the best monospaced font out there. I use it for all monospaced text
* [Liberation](https://github.com/liberationfonts/liberation-fonts/): backup font for anything Noto doesn't look good on
* [Noto](https://fonts.google.com/noto/): pan-Unicode font. I have some qualms with it, but the fact that it covers
  almost everything in Unicode means I don't have to fuss with fontconfig too much, and having one really consistent
  font helps make my system look homogenous. And hey, full CJK support!
* grimshot: screenshot tool

## Nonfree programs I use
I tend not to condone the use of proprietary and/or freedom-disrespecting software. Unfortunately, the industry standard
is not caring about the end user. Some corporate overlords are worse than others, but nonfree is nonfree any way you
slice it. That all said, I am a digital citizen. My life is centered around technology in almost every way (for better
at times but usually for worse). Thus, I've allowed myself to use several nonfree programs. Most of them aren't worth
pointing out because they're either webapps or necessary for work. I will draw attention to only these:

* Steam: games library, installed for the use of:
  + The games themselves: entertainment

I usually do not launch games from Steam's mediocre client. I use [Vapor](https://gitlab.com/Seltyk/vapor/).
