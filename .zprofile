#Runs when I log into this account
#Dunno why I'm putting these here but w/e
export VISUAL=vim
export EDITOR=$VISUAL

#Use fcitx for input. It's great in a lot of ways and shabby in few. Worth it
#Whether this has any effect is still kinda "i dunno", especially now that I use fcitx5
export GTK_IM_MODULE=fcitx
export QT_IM_MODULE=fcitx
export XMODIFIERS=@im=fcitx

#bemenu: case-insensitive, list 5 closest matches, prompt with angle bracket
export BEMENU_OPTS="-il 5 -P '>'"

#Enable Wayland w/ Qt5
export QT_QPA_PLATFORM=wayland-egl
#export QT_QPA_PLATFORMTHEME=gtk3
export QT_QPA_PLATFORMTHEME=qt5ct

#Use Wayland w/ Firefox
#export MOZ_ENABLE_WAYLAND=1
export MOZ_DBUS_REMOTE=1

#Add scripts, Cargo, and JDK to the PATH
export PATH="$PATH:$HOME/Scripts:$CARGO_HOME/bin:$HOME/.local/bin:$HOME/OpenJDK/17.0.8_7-hotspot/bin:/usr/lib/psql15/bin"

#Set my time zone to Eastern
export TZ="America/New_York"

#Make sure configs are configs, ya know?
export XDG_CONFIG_HOME="$HOME/.config"

#Use asynchronous shader compilation in GE's Proton
export DXVK_ASYNC=1

#Add colors to less/man
#Yoinked from Luke Smith's GitLab
export LESS=-RI
export LESS_TERMCAP_mb="$(printf '%b' '[1;31m')"
export LESS_TERMCAP_md="$(printf '%b' '[1;36m')"
export LESS_TERMCAP_me="$(printf '%b' '[0m')"
export LESS_TERMCAP_so="$(printf '%b' '[01;44;33m')"
export LESS_TERMCAP_se="$(printf '%b' '[0m')"
export LESS_TERMCAP_us="$(printf '%b' '[1;32m')"
export LESS_TERMCAP_ue="$(printf '%b' '[0m')"
export LESSOPEN="| /usr/bin/highlight -O ansi %s 2> /dev/null"

#Grab zshrc stuff
[[ -f $HOME/.zshrc ]] && . "$HOME/.zshrc"

#Start sway on a session bus
[[ -z "$DISPLAY" && "$(tty)" = "/dev/tty1" ]] && exec env XDG_CURRENT_DESKTOP=sway dbus-run-session sway
